package model

import kotlinx.serialization.Serializable

@Serializable
data class JocProves(val input:String, val ouput:String)