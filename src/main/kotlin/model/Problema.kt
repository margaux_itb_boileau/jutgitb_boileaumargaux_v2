package model

import kotlinx.serialization.Serializable
import java.time.LocalDate

@Serializable
class Problema(val titol:String, val enunciat:String, val jocPubl:JocProves, val jocPriv:JocProves, val tipus:String) {
    val llistaIntents: MutableList<Intent> = mutableListOf()
    var intents=0
    var estat:Boolean = false

    fun corregeix(resposta: String) {
        llistaIntents.add(Intent(resposta, LocalDate.now().toString()))
        intents++
        estat = (resposta == jocPriv.ouput)
    }

}