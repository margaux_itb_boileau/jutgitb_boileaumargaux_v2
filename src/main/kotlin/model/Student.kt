package model

import kotlinx.serialization.Serializable

@Serializable
class Student(val name: String, val password: String){
    var historial = mutableListOf<MutableList<Problema>>()
}