package model

import Admin

class Profe(val name: String, val password: String): Admin {

    override fun addProblem(problem: Problema, problemsList: MutableList<MutableList<Problema>>):MutableList<MutableList<Problema>> {
        for (llista in problemsList) {
            if (llista[0].tipus==problem.tipus) {
                llista.add(problem)
                return problemsList
            }

        }
        problemsList.add(mutableListOf(problem))
        return problemsList
    }

    override fun createReport(historial: MutableList<MutableList<Problema>>):String {
        var report = "┏━━━━━━━━━━┳━━━━━━━━━┳━━━━━━━━┓\n"
        report +="┃ PROBLEMA ┃ INTENTS ┃ RESOLT ┃\n"

        var fets = 0
        var notes = mutableListOf<Int>()
        for (i in historial.indices) {
            for (j in historial[i].indices) {
                var color: String  //yellow
                var nota = 10
                var resolt = "no"
                if (historial[i][j].estat) {
                    color = "\u001B[32m" //green
                    resolt = "si"
                    fets++
                    when (historial[i][j].intents) {
                        1 -> nota = nota
                        2 -> nota -= 1
                        3 -> nota -= 2
                        4 -> nota -= 3
                        5 -> nota -= 4
                        else -> nota -= 5
                    }
                    notes.add(nota)
                }
                else if(historial[i][j].intents==0) color = "\u001B[31m" //red
                else color = "\u001B[33m" //yellow
                report+="┣━━━━━━━━━━╋━━━━━━━━━╋━━━━━━━━┫\n"
                report+="┃   $color${i+1}.${j+1}${"\u001B[0m"}    ┃    ${historial[i][j].intents}    ┃   $resolt   ┃\n"
            }
        }
        report+="┗━━━━━━━━━━┻━━━━━━━━━┻━━━━━━━━┛\n"
        val mitjana = if (fets!=0) notes.sum()/fets
        else 0
        report+= "Nota mitjana de problemes resolts: $mitjana"
        return report
    }

}