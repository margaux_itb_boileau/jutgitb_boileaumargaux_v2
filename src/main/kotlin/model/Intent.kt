package model

import kotlinx.serialization.Serializable

@Serializable
data class Intent(var resposta:String, var data:String)