/*
* AUTHOR: Margaux Boileau
* DATE: 2023/3/12
* TITLE: JutgITB
* VERSION: 2.0
*/

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import model.*
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readText
import kotlin.io.path.writeText
import kotlin.system.exitProcess

val scanner = Scanner(System.`in`)
val prof = Profe("Jordi", "ITB")
var student = getUser()
var problemsList = getProblems()

fun main() {
    val user = login()
    if (user is Profe) {
        println("Benvingut professor ${user.name}")
        menuProfessor()
    }
    else if (user is Student) {
        println("Benvingut estudiant ${user.name}")
        menuStudent()
    }
}

//FILES/DATA MANAGEMENT
fun createNewStudent(){
    println("Nom:")
    val name= scanner.next()
    println("Contrasenya:")
    val password= scanner.next()
    val newStudent = Student(name,password)
    newStudent.historial=getProblems()
    updateUserData(newStudent)
}
fun updateUserData(student:Student) {
    val path = Path("./src/main/kotlin/data/user.json")
    path.writeText(Json.encodeToString(student))
}
fun createProblem(): Problema {
    println("Creació de nou problema:")
    val words = arrayListOf("Secció (pot ser nova o existent)","Títol","Enunciat","Input public","Output public","Input privat","Output privat")
    val elements = mutableListOf<String>()
    for (word in words) {
        println(word)
        val element = scanner.next()
        elements.add(element)

    }
    val problema = Problema(elements[1], elements[2], JocProves(elements[3],elements[4]), JocProves(elements[5],elements[6]),elements[0])
    return problema
}
fun getProblems(): MutableList<MutableList<Problema>> {
    val path = Path("./src/main/kotlin/data/problems.json")
    val content = path.readText()
    val problems = Json.decodeFromString<MutableList<MutableList<Problema>>>(content)
    return problems
}
fun updateProblems(problems: MutableList<MutableList<Problema>>) {
    val path = Path("./src/main/kotlin/data/problems.json")
    path.writeText(Json.encodeToString(problems))
}
fun getUser(): Student {
    val path = Path("./src/main/kotlin/data/user.json")
    if (path.toFile().length()==0L) {
        println("Encara no hi ha cap estudiant registrat, abans de realitzar cap acció hauràs de crear-ne un")
        createNewStudent()
    }
    val content = path.readText()
    val stdnt = Json.decodeFromString<Student>(content)
    return stdnt
}
//LOGIN
fun login():Any {
    println("INICI DE SESIÓ")
    val user = getUserInfo()
    if (user is Profe) {
        if (user.password == prof.password && user.name == prof.name) return prof
        else return login()
    }
    else if (user is Student) {
        if (user.password == student.password && user.name == student.name) return student
        else return login()
    }
    else {
        println("Wrong")
        return login()
    }
}
fun getUserInfo(): Any {
    println("Quin rol tens?\n" +
            "1.Professor\n" +
            "2.Estudiant\n")
    val role= scanner.next()
    println("Nom:")
    val name= scanner.next()
    println("Contrasenya:")
    val password= scanner.next()
    return if (role=="1") Profe(name,password)
    else Student(name,password)
}
//MENU FUNCTIONS
fun menuProfessor() {
    println("Què vols fer?\n" +
            "1. Afegir un nou problema al llistat\n" +
            "2. Generar un informe de les notes del estudiant\n" +
            "3. Sortir\n")
    val action = scanner.next()
    when (action) {
        "1"-> {
            println("A continuació podràs afegir un nou problema al llistat, el pots afegir a una secció ja existent (inserint el nom d'aquesta) o crear una de nova.\nVols veure les seccions i problemes actuals abans d'afegir-ne un de nou?\n" +
                    "1. Sí\n" +
                    "2. No\n")
            if (scanner.next()=="1") printList(problemsList)
            val newProblem = createProblem()
            problemsList = prof.addProblem(newProblem, problemsList)
            updateProblems(problemsList)
            student.historial=prof.addProblem(newProblem, student.historial)
            updateUserData(student)
        }
        "2"-> {
            println("A continuació es crearà una taula dels problemes existents (indicats pel seu numero) amb un camp que indica els intents realitzats i un altre que mostra si està resolt o no.\n" +
                    "Els numeros dels problemes poden aparèixer en 3 colors:\n" +
                    "· Verd si està resolt\n" +
                    "· Vermell si no està començat\n" +
                    "· Groc si té algun intent però no està resolt encara\n" +
                    "\n" +
                    "Vols continuar?\n" +
                    "1. Sí\n" +
                    "2. No")
            if (scanner.next()=="2") menuProfessor()
            val report = prof.createReport(student.historial)
            println(report)
        }
        "3"-> exitProcess(0)
    }
    println()
    menuProfessor()
}
fun menuStudent() {
    println()
    println("Què vols fer?\n"+
            "1. Seguir amb l’itinerari d’aprenentatge\n" +
            "2. Llista problemes\n" +
            "3. Consultar històric de problemes\n" +
            "4. Ajuda\n" +
            "5. Sortir\n")
    do {
        val instruccio = scanner.next()
        when (instruccio) {
            "1"-> resume()
            "2"-> choose()
            "3"-> printHistorial(student.historial)
            "4"-> printHelp()
            "5"-> {
                updateUserData(student)
                exitProcess(0)
            }
        }
        menuStudent()
    } while (true)
}
fun resume() {
    for (llista in student.historial) {
        for (problema in llista){
            if (!problema.estat) logica(problema)
        }
    }
}
fun choose() {
    printList(student.historial)
    do {
        println("Selecciona la secció per número")
        val sectionIndex = scanner.nextInt()
        println("Selecciona el problema per número")
        val problemIndex = scanner.nextInt()
        if (sectionIndex in student.historial.indices) {
            if (problemIndex in student.historial[sectionIndex].indices) {
                val problema = student.historial[sectionIndex-1][problemIndex-1]
                if (problema.estat) println("Aquest problema ja està resolt, escull un altre\n")
                else logica(problema)
            }
        }
        else println("Els números inserits no són vàlids\n")
    } while (true)
}
fun logica(problema: Problema) {
    do {
        printProblem(problema)
        println("Entrada: ${problema.jocPriv.input}")
        val resposta = scanner.next()
        problema.corregeix(resposta)
        if (!problema.estat) {
            println("Resposta incorrecta")
            println("\t1.Tornar a intentar\n\t2.Següent exercici\n\t3.Tornar al menu")
            val instruccio = scanner.next()
            when(instruccio){
                "1"->continue
                "2"-> break
                else->menuStudent()
            }
        }
        else {
            println("Resposta correcta")
            println("\t1.Següent exercici \n\t2.Tornar al menu")
            val instruccio = scanner.next()
            when(instruccio){
                "1"-> break
                else -> menuStudent()
            }
        }
    } while (true)
}
//PRINTS
fun printProblem(problem:Problema) {
    println(problem.titol)
    println(problem.enunciat)
    println("Entrada: ${problem.jocPubl.input}")
    println("Sortida: ${problem.jocPubl.ouput}")
}
fun printList(problems:MutableList<MutableList<Problema>>) {
    for (i in problems.indices) {
        println("${i + 1} ${problems[i][0].tipus.uppercase()}")
        for (j in problems[i].indices){
            println("${i + 1}.${j + 1} ${problems[i][j].titol}${problems[i][j].enunciat}")
        }
        println()
    }
}
fun printHistorial(problems: MutableList<MutableList<Problema>>) {
    for (i in problems.indices) {
        println("${i + 1}. ${problems[i][0].tipus.uppercase()}")
        for (j in problems[i].indices){
            println("${i + 1}.${j + 1} ${problems[i][j].titol}")
            println("\tIntents: ${problems[i][j].intents}")
            if (problems[i][j].estat) println("\tResolt: si")
            else println("\tResolt: no")
        }
        println()
    }
}
fun printHelp() {
    println("Benvingut a jutgITB!\n" +
            "En iniciar el programa podras iniciar sessió com a alumne o professor. \n" +
            "L'alumne podrà resoldre els problemes de forma lineal o escollint els que vol, també podrà veure el seu historial de problemes amb el nombre d'intents de cada i si aquest està resolt. \n" +
            "El professor podrà actualitzar el llistat de problemes i generar un informe del treball de l'alumne per veure el progrés d'aquest i la seva nota mitjana dels problemes resolts.\n" +
            "Per a navegar entre les diferents opcions, hauràs d'escriure per terminal el numero de l'opció que vols escollir.\n")
}