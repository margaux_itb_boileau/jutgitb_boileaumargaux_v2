import model.Problema

interface Admin {
    fun addProblem(problem: Problema, problemsList:MutableList<MutableList<Problema>>):MutableList<MutableList<Problema>>
    fun createReport(historial:MutableList<MutableList<Problema>>):String
}